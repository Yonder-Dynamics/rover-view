import React, {Component} from 'react';
import ReactPlayer from 'react-player';
import ReactDOM from 'react-dom';

class Player extends React.Component {
  render() {
    return (
      <div className='player-wrapper'>
      <ReactPlayer
        url = 'https://www.youtube.com/watch?v=VUA2Rfvra64'
        playing
        className='react-player'
        width='100%'
        height='100%'
        />
        </div>
    )
  }
}



export default Player;
